<?php

	include('config/db_connect.php');
	include('item.php');
	include('book.php');
	include('furniture.php');
	include('dvd.php');
	require('validator.php');

	$errors = [];
	$args = [];

	if(isset($_POST['submit'])){
		$validation = new Validator($_POST);
		$type = $_POST['productType'];
		$errType = 'validate' . $type;
		$errors = $validation->$errType();
		if(!array_filter($errors)){
		$args = $_POST;
		unset($args['productType'], $args['submit']);
		$args = array_values($args);
		$item = new $type(...$args);
		$item->add('product', $pdo);
		header('Location: index.php');
	  }
	}
	
?>

<!DOCTYPE html>
<html>
	<?php include('templates/header2.php'); ?>
	<?php include('templates/addForm.php'); ?>
	<?php include('templates/footer.php'); ?>

</html>