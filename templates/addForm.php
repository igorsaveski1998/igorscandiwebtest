<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="scripts/dynamicFormScript.js"></script>
			
<section class="container grey-text">
	<h4 class="center">Product Add</h4>
	<form class="white" action="add-product.php" method="POST" id="product_form">
		<div>
			<label>SKU</label>
			<input type="text" id="sku" name="sku" value="">
			<div class="red-text"><?php echo $errors['sku'] ?? '' ?></div>
		</div>
		<label>Name</label>
		<input type="text" id="name" name="name" value="">
		<div class="red-text"><?php echo $errors['name'] ?? '' ?></div>
		<label>Price ($)</label>
		<input type="text" id="price" name="price" value="">
		<div class="red-text"><?php echo $errors['price'] ?? '' ?></div>
		<label>Type Switcher</label>
		<select class="browser-default" name="productType" id="productType" onchange="changeType(this.id)">
			<option value="Item" selected></option>
			<option value="Dvd">DVD</option>
			<option value="Furniture">Furniture</option>
			<option value="Book">Book</option>
		</select>
		<div id="container">
			<div class="red-text">
				<?php echo $errors['productType'] ?? '' ?>
				<?php echo $errors['size'] ?? '' ?>
				<?php echo $errors['weight'] ?? '' ?>
				<?php echo $errors['height'] ?? '' ?>
				<br>
				<?php echo $errors['width'] ?? '' ?>
				<br>
				<?php echo $errors['length'] ?? '' ?>
				
			</div>
		</div>
		<div class="center">
			<input type="submit" name="submit" value="Save" class="btn brand z-depth-0">
			<input type="button" name="submit" value="Cancel" class="btn brand z-depth-0" onClick="document.location.href='index.php';">
		</div>
	</form>
</section>