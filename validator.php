<?php 

class Validator {

  private $data;
  private $errors = [];
  private static $fields = ['sku', 'name', 'price', 'productType'];
  private static $dvdFields = ['sku', 'name', 'price', 'size'];
  private static $furnitureFields = ['sku', 'name', 'price', 'height', 'width', 'length'];
  private static $bookFields = ['sku', 'name', 'price', 'weight'];

  public function __construct($post_data){
    $this->data = $post_data;
  }

  public function validateItem(){
    foreach(self::$fields as $field){
      if(!array_key_exists($field, $this->data)){
        trigger_error("'$field' is not present in the data");
        return;
      }
    }
    $this->validateSku();
    $this->validateName();
    $this->validatePrice();
    $this->validateType();
    return $this->errors;
  }

  public function validateDvd(){
    foreach(self::$dvdFields as $field){
      if(!array_key_exists($field, $this->data)){
        trigger_error("'$field' is not present in the data");
        return;
      }
    }
    $this->validateSku();
    $this->validateName();
    $this->validatePrice();
    $this->validateSize();
    return $this->errors;
  }

  public function validateFurniture(){
    foreach(self::$furnitureFields as $field){
      if(!array_key_exists($field, $this->data)){
        trigger_error("'$field' is not present in the data");
        return;
      }
    }
    $this->validateSku();
    $this->validateName();
    $this->validatePrice();
    $this->validateHeight();
    $this->validateWidth();
    $this->validateLength();
    return $this->errors;
  }

  public function validateBook(){
    foreach(self::$bookFields as $field){
      if(!array_key_exists($field, $this->data)){
        trigger_error("'$field' is not present in the data");
        return;
      }
    }
    $this->validateSku();
    $this->validateName();
    $this->validatePrice();
    $this->validateWeight();
    return $this->errors;
  }

  private function validateType(){
    $val = trim($this->data['productType']);
    if(empty($val) || $val=="Item"){
      $this->addError('productType', 'Please, submit required data');}}

  private function validateSku(){
        $val = trim($this->data['sku']);
        if(empty($val)){
          $this->addError('sku', 'Please, submit required data');
        } else {
          if(!preg_match('/^[a-zA-Z0-9\s]+$/', $val)){
            $this->addError('sku','Please, provide the data of indicated type');
          }
        }
  }    

  private function validateName(){
        $val = trim($this->data['name']);
        if(empty($val)){
          $this->addError('name', 'Please, submit required data');
        } else {
          if(!preg_match('/^[a-zA-Z0-9\s]+$/', $val)){
            $this->addError('name','Please, provide the data of indicated type');
          }
        }
  }   

  private function validatePrice(){
        $val = trim($this->data['price']);
        if(empty($val)){
          $this->addError('price', 'Please, submit required data');
        } else {
          if(!preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $val)){
            $this->addError('price','Please, provide the data of indicated type');
          }
        }
  }     

  private function validateSize(){
        $val = trim($this->data['size']);
        if(empty($val)){
          $this->addError('size', 'Please, submit required data');
        } else {
          if(!preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $val)){
            $this->addError('size','Please, provide the data of indicated type');
          }
        }
  }    

  private function validateHeight(){
        $val = trim($this->data['height']);
        if(empty($val)){
          $this->addError('height', 'Please, submit required data');
        } else {
          if(!preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $val)){
            $this->addError('height','Please, provide the data of indicated type');
          }
        }
  }   

  private function validateWidth(){
        $val = trim($this->data['width']);
        if(empty($val)){
          $this->addError('width', 'Please, submit required data');
        } else {
          if(!preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $val)){
            $this->addError('width','Please, provide the data of indicated type');
          }
        }
  }    

  private function validateLength(){
        $val = trim($this->data['length']);
        if(empty($val)){
          $this->addError('length', 'Please, submit required data');
        } else {
          if(!preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $val)){
            $this->addError('length','Please, provide the data of indicated type');
          }
        }
  }    

   private function validateWeight(){
        $val = trim($this->data['weight']);
        if(empty($val)){
          $this->addError('weight', 'Please, submit required data');
        } else {
          if(!preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $val)){
            $this->addError('weight','Please, provide the data of indicated type');
          }
        }
  }   

  private function addError($key, $val){
        $this->errors[$key] = $val;
  }    

}

  ?>