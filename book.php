<?php 

class Book extends Item {

    private $weight;

    public function __construct($sku, $name, $price, $weight){
         parent::__construct($sku, $name, $price);
         $this->weight = $weight; 
    }

    public function add($table, $pdo){
     $sql = 'INSERT INTO ' . $table . ' (sku, name, price, attribute, value) VALUES(:sku, :name, :price, :attribute, :value)';
     $stmt = $pdo->prepare($sql);
     $stmt->execute(['sku' => $this->sku, 
                    'name' => $this->name, 
                    'price' => $this->price, 
                    'attribute' => 'Weight', 
                    'value' => $this->weight . ' KG']);
     }

}

?>