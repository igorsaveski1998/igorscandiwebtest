function changeType(s1){
var container = document.getElementById("container");
	while (container.hasChildNodes()) {
		container.removeChild(container.lastChild);
	}
	var s1 = document.getElementById(s1);
	if(s1.value == "Dvd"){
		var label = document.createElement("label");
		label.innerHTML = "Size (MB)";
		container.appendChild(label);
		var input = document.createElement("input");
		input.type = "text";
		input.name = "size";
		input.id = "size";
		container.appendChild(input);		
        var para = document.createElement("P");            
        para.innerText = "Please provide size in MB";
        container.appendChild(para);  
    } else if(s1.value == "Furniture"){
        var labelH = document.createElement("label");
        labelH.innerHTML = "Height (CM)";
        container.appendChild(labelH);
        var inputH = document.createElement("input");
        inputH.type = "text";
        inputH.name = "height";
        inputH.id = "height";
        container.appendChild(inputH);
        var labelW = document.createElement("label");
        labelW.innerHTML = "Width (CM)";
        container.appendChild(labelW);
        var inputW = document.createElement("input");
        inputW.type = "text";
        inputW.name = "width";
        inputW.id = "width";
        container.appendChild(inputW);
        var labelL = document.createElement("label");
        labelL.innerHTML = "Length (CM)";
        container.appendChild(labelL);
        var inputL = document.createElement("input");
        inputL.type = "text";
        inputL.name = "length";
        inputL.id = "length";
        container.appendChild(inputL);
		var para = document.createElement("P");              
		para.innerText = "Please provide dimensions in HxWxL format";               
		container.appendChild(para);     
	} else if(s1.value == "Book"){
		var label = document.createElement("label");
		label.innerHTML = "Weight (KG)";
		container.appendChild(label);
		var input = document.createElement("input");
		input.type = "text";
		input.name = "weight";
		input.id = "weight";
		container.appendChild(input);
        var para = document.createElement("P");          
		para.innerText = "Please provide weight in KG";               
		container.appendChild(para); 
	}					

					

}

