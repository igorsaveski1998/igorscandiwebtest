$(document).ready(function(){
	$('#delete-product-btn').click(function(){
		var id = [];
		$('.delete-checkbox:checked').each(function(i){
			id[i] = $(this).val();
		});
		$.ajax({
			url:'delete.php',
			method:'POST',
			data:{id:id},
			success:function(){
				for(var i=0; i<id.length; i++){
					var elem = document.getElementById(id[i]);
					elem.parentNode.removeChild(elem);
				}
			}
		});
	});
});