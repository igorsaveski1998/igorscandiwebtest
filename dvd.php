<?php 

class Dvd extends Item {

    private $size;

    public function __construct($sku, $name, $price, $size){
         parent::__construct($sku, $name, $price);
         $this->size = $size; 
    }


    public function add($table, $pdo){
     $sql = 'INSERT INTO ' . $table . ' (sku, name, price, attribute, value) VALUES(:sku, :name, :price, :attribute, :value)';
     $stmt = $pdo->prepare($sql);
     $stmt->execute(['sku' => $this->sku,
                     'name' => $this->name, 
                     'price' => $this->price, 
                     'attribute' => 'Size', 
                     'value' => $this->size . ' MB'] );  
     }

}

?>