<?php 

abstract class Item {

	protected $sku;
	protected $name;
	protected $price;

	public function __construct($sku, $name, $price){
		$this->sku = $sku;
		$this->name = $name;
		$this->price = $price;
	}

	public static function displayAll($table, $pdo){
		$sql = 'SELECT * FROM ' . $table;
		$stmt = $pdo->prepare($sql);
		$stmt->execute();
		return $stmt->fetchAll();
	}

	abstract protected function add($table, $pdo);

	public static function delete($table, $pdo, $id){
		$sql = 'DELETE FROM ' . $table . ' WHERE id=?';
		$stmt= $pdo->prepare($sql);
		$stmt = $pdo->prepare($sql);
		$stmt->execute([$id]);
	}

}

?>