<?php 

include('config/db_connect.php');
include('item.php');
//include('book.php');

$items = Item::displayAll('product', $pdo);
//$itemOne = new Book('sku', 'name', '2', '2kg');
//$itemOne->add('product', $pdo);
//Item::delete('product', $pdo, '22');

?>

<!DOCTYPE html>
<html>
	<?php include('templates/header.php'); ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="scripts/deleteScript.js"></script>

	<button id="delete-product-btn" class="btn brand z-depth-0">MASS DELETE</button>
			</ul>
		</div>
	</nav>

	<div class="container">
		<div class="row">
			<?php foreach ($items as $item): ?> 
				<div class="col s3 md3">
					<div class="card z-depth-0" id="<?php echo $item->id; ?>">
						<label class="delete-checkbox">
						<input type="checkbox" name="delete-checkbox[]" class="delete-checkbox" value=<?php echo $item->id; ?> />
						<span></span>
						</label>
						<div class="card-content center">
							<ul>
								<li><?php echo $item->sku; ?></li>
								<li><?php echo $item->name; ?></li>
								<li><?php echo number_format($item->price,2) .' $'; ?></li>
								<li><?php echo $item->attribute . ': '. $item->value; ?></li>
							</ul>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>

<?php include('templates/footer.php'); ?>
</html>

