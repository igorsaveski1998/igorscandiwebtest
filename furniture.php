<?php 

class Furniture extends Item {

    private $height;
    private $width;
    private $length;

    public function __construct($sku, $name, $price, $height, $width, $length){
         parent::__construct($sku, $name, $price);
         $this->height = $height;
         $this->width = $width;
         $this->length = $length; 
    }

    public function add($table, $pdo){
     $sql = 'INSERT INTO ' . $table . ' (sku, name, price, attribute, value) VALUES(:sku, :name, :price, :attribute, :value)';
     $stmt = $pdo->prepare($sql);
     $stmt->execute(['sku' => $this->sku, 
                    'name' => $this->name, 
                    'price' => $this->price, 
                    'attribute' => 'Dimension', 
                    'value' => $this->height . 'x' . $this->width . 'x' . $this->length]);
           // echo 'Post Added';
     }

}

?>